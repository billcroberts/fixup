/*
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

/*
 * sample:
 *   $ gcc -o ioctl ioctl.c
 *   $ ./ioctl 5879
 *    type: "X"
 *    nr: "121"
 */
#include <stdlib.h>
#include <stdio.h>
#include <asm-generic/ioctl.h>


void usage(char *self) {
	fprintf(stderr, "%s: <ioctlcmd>\n"
		"\t Where ioctlcmd is the ioctlcmd reported from SELinux audit messages\n"
		"Output:\n"
		"\tThe type field, in character output. By convention, kernel ioctl type fields are a \"random\" character.\n"
		"\tThe nr field, in base 10 output. By convention, kernel ioctl nr fields are base10.\n"
		"Returns:\n"
		"\t0 on success\n", self);

	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {

	int nr;
	int type;
	int ionum;
	char *endptr;

	if (argc != 2) {
		usage(argv[0]);
	}

	ionum = strtol(argv[1], &endptr, 16);
	if (*endptr != '\0') {
		fprintf(stderr, "Could not convert input text: %s", argv[1]);
		usage(argv[1]);
	}

	type = _IOC_TYPE(ionum);
	nr = _IOC_NR(ionum);
	printf("type: \"%c\"\n", type);
	printf("nr: \"%d\"\n", nr);

	exit(EXIT_SUCCESS);
}
